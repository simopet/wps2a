=== WP send to Analytics ===
Contributors: simone.p
Tags: Analytics, events, tracking
Requires at least: 4.0.1
Tested up to: 4.8
Stable tag: 4.3
Donate: https://www.paypal.me/simonepetrucci
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
 
WP send to Analytics lets you monitor user interactions with the content (pages, posts)
 
== Description ==
 
**This plugin uses the universal Google Analytics script.**
**If you're implementing a different version of Google Analytics, or you have a custom tracker, WP send to Analytics will not work properly.**

The plugin automatically traks some user behaviours and sends a hit (event) to Google Analytics:

- By default, the plugin sends an event when the user reaches the end of the content, including a "Value" in seconds, according to the time it takes the user to reach the end, from the first scrolling.
 
- You can ask the plugin to track the view of a custom selector, instead of the end-content one, by simply give that ID in the custom box in your posts and pages.

 
== Installation ==
 
1. Unzip
2. copy the folder into your wp-content/plugins folder
3. go back to your admin panel => plugins and activate wps2a
4. you will receive a warning because wps2a needs a free plugin called "Meta Box", follow the instructions and install that plugin too.
5. Be sure you're already using Google Analytics script
6. That's all. The plugin is working. If you want to monitor a custom element on your pages/posts, use the box in the sidebar to tell the plugin what to look for: if you write "custom_selector", the plugin will send Analytics an event type hit when user will reach an element with id="custom_selector" attribute.

 
== Frequently Asked Questions ==
 
= Does it work with all the Google Analytics scripts? =
 
Nope, WP send to Analytics works with analytics.js version
 
= What happens if I don't use the metabox for jQuery/CSS selector? =
 
The plugins will be monitoring the content, and will push an event on Google Analytics when the user reaches its end.
 
== Screenshots ==
 

 
== Changelog ==
 
= 1.1 =
* Added Option Panel
 
= 1.0 =
* All seems to work properly
 
 
== Upgrade Notice ==
 
= 1.0 =



